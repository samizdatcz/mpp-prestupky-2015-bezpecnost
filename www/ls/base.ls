ig = window.ig
init = ->
  tooltip = new Tooltip!watchElements!
  [defaultTypId, location] = window.location.hash.substr 1 .split ':'
  defaultTypId = parseInt defaultTypId, 10
  ig.defaultTypId = defaultTypId || null
  ig.dir = dir = "prestupky"
  ig.isRychlost = no
  container = d3.select ig.containers.base
  if ig.isRychlost then container.classed \rychlost yes
  map = new ig.Map ig.containers.base
    ..drawHeatmap dir

  (err, data) <~ d3.text "../data/processed/#dir/typy.tsv"
  ig.typy = typy = for text, id in data.split "\n"
    {text, id}
  infobar = new ig.Infobar container, typy
  new ig.TypSelector container, map, infobar, typy
  map
    ..on \selection infobar~draw
    ..on \selectionComplete infobar~selectionComplete
    ..on \markerClicked infobar~clearFilters
  heatmapLastPointList = null
  mapTimeout = null
  lastHeatCall = 0
  if not defaultTypId and infobar.element.node!offsetLeft != window.innerWidth
    startLatlng = L.latLng [50.092172, 14.398612]
    endLatlng = L.latLng [50.073, 14.442]
    map
      ..selectionRectangle.setBounds [startLatlng, endLatlng]
      ..setSelection [[startLatlng.lat, startLatlng.lng], [endLatlng.lat, endLatlng.lng]]

  throttleHeatmap = (pointList) ->
    heatmapLastPointList := pointList
    return if mapTimeout isnt null
    nextCall = Math.max do
      lastHeatCall - Date.now! + 500
      0
    mapTimeout := setTimeout do
      ->
        map.drawFilteredHeatmap heatmapLastPointList
        lastHeatCall := Date.now!
        mapTimeout := null
      nextCall

  infobar
    ..on \updatedPoints throttleHeatmap
    ..on \selectionCancelled map~cancelSelection
  geocoder = new ig.Geocoder ig.containers.base
    ..on \latLng (latlng) ->
      map.map.setView latlng, 18
      map.onMapChange!

  new ig.EmbedLogo ig.containers.base, dark: yes
  handleHashLocation = (hashLocation) ->
    [lat, lon, zoom] = hashLocation.split /[^-\.0-9]+/
    lat = parseFloat lat
    lon = parseFloat lon
    zoom = parseFloat zoom
    if lat and lon and zoom >= 0
      map.map.setView [lat, lon], zoom


  if location
    handleHashLocation location

  window.onhashchange = ->
    [dir, location] = window.location.hash.substr 1 .split ':'
    if location
      handleHashLocation location
if d3?
  init!
else
  $ window .bind \load ->
    if d3?
      init!

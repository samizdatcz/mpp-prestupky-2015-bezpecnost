class ig.TypSelector
  (@parentElement, @map, @infobar, typy) ->
    @typy = typy.slice 1
    @typy.unshift {id: null, text: "Všechny přestupky"}
    map = @map
    infobar = @infobar
    @parentElement.append \select
      ..attr \class \typ-selector
      ..selectAll \option .data @typy .enter!append \option
        ..attr \value (.id)
        ..html (.text)
        ..attr \selected -> if it.id is ig.defaultTypId then "selected" else void
      ..on \change ->
        map.setHeatmapFilter parseInt @value, 10
        infobar.clearSelection!
